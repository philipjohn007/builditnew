var app = angular.module('project');

app.controller('PlantsController', function($scope, $http) {
	$scope.plants = [];
	$http.get('/rest/plants').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.plants = data;
	});	
});

app.run(function($rootScope) {
});

app.controller('POController', function($scope, $http) {
	$scope.pos = [];
	$http.get('/rest/pos').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.pos = data;
	});	
});

app.controller('InvoiceController', function($scope, $http, $route, $location, $rootScope) {
	$scope.invoices = [];
	$scope.po=null;
	$scope.poShown=false;
	$http.get('/rest/invoices').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.invoices = data;
	});	
	
	$scope.follow = function(link) {
		$scope.poShown=false;
	    console.log(link);
	    console.log(link.method);
	    var users = $rootScope.roles;
	    console.log($rootScope.roles);
	    if (users.indexOf('ROLE_SITE') != -1) {
	    	if (link.method == 'POST') {
	    		$http.post(link.href).success(function(data, status, headers, config) {
	    			console.log(data);
	    			$route.reload();
	    		});
	    	} else if (link.method == 'DELETE') {
	    		$http.delete(link.href).success(function(data, status, headers, config) {
	    			console.log(data);
	    			$route.reload();
	    		});
	    	}
	    } else {
			alert('Not Authorised!');
	    }
	};
	
	$scope.findPO = function(link) {
		var poId = link.substring(link.lastIndexOf("/")+1, link.length);
		$http.get('/rest/pos/'+poId).success(function(data, status, headers, config) {
			console.log(JSON.stringify(data));
			$scope.po = data;
			$scope.poShown=true;
		});	
	};
});

app.controller('NotificationController', function($scope, $http) {
	$scope.notifications = [];
	$scope.po=null;
	$scope.poShown=false;
	$http.get('/rest/notifications').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.notifications = data;
		$scope.poShown=false;
	});	
	
	$scope.findPO = function(link) {
		var poId = link.substring(link.lastIndexOf("/")+1, link.length);
		$http.get('/rest/pos/'+poId).success(function(data, status, headers, config) {
			console.log(JSON.stringify(data));
			$scope.po = data;
			$scope.poShown=true;
		});	
	};
});

app.controller('PHRController', function($scope, $http, $route, $location, $rootScope, $cookieStore) {
	
	$scope.phrs = [];
	$scope.plant=null;
	$scope.plantShown=false;
	$http.get('/rest/phrs').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.phrs = data;
	});
	$scope.follow = function(link,phr) {
		$scope.plantShown=false;
	    console.log(link);
	    console.log(link.method);
	    var users = $rootScope.roles;
	    console.log(users);
	    if (link.method == 'POST') {
	    	if (users.indexOf('ROLE_WORK') != -1)
    		{   $http.post(link.href).success(function(data, status, headers, config) {
    				console.log(data);
    				$route.reload();
	        	});
    		} else {
    			alert('Not Authorised!');
    		}
	    } else if (link.method == 'DELETE') {
	    	if(link.rel == 'Cancel') {
	    		if (users.indexOf('ROLE_SITE') != -1) {
	    			var currentDate = new Date();
	    			console.log(currentDate);
	    			var start = new Date(phr.startDate);
	    			console.log(start);
	    			console.log(phr.startDate);
	    			if(start > currentDate) {
	    				$http.delete(link.href).success(function(data, status, headers, config) {
	    					console.log(data);
	    					$route.reload();
	    				});
	    			} else {
	    				alert('Cancel Not Possible Now!');
	    			}
	    		} else {
	    			alert('Not Authorised!');
	    		}
	    	} else {
	    		if (users.indexOf('ROLE_WORK') != -1) {
	    			$http.delete(link.href).success(function(data, status, headers, config) {
	    				console.log(data);
	    				$route.reload();
	    			}); 
	    		} else {
	    			alert('Not Authorised!');
	    		}
	    	}
	    } else if (link.method == 'PUT') {
	    	if(link.rel == 'Extend') {
	    		console.log(link.rel);
	    		if (users.indexOf('ROLE_SITE') != -1) {
	    			var url = link.href;
    				var newUrl = url.substring(0, url.length - 7);
	    			$http.get(newUrl).success(function(data, status, headers, config) { 
	    				$rootScope.getUrl = newUrl;
	    				$rootScope.extendUrl = link.href;         			
	    				console.log(link.href);
	    				$location.path('/extend');
	    			}); 
	    		} else {
	    			alert('Not Authorised!');
	    		}
	    	} else {
	    		
	    		
	    		if (users.indexOf('ROLE_WORK') != -1 || users.indexOf('ROLE_SITE') != -1) {
	    			$http.get(link.href).success(function(data, status, headers, config) { 
	    				$rootScope.updateUrl = link.href;         			
	    				console.log(link.href);
	    				$location.path('/update');
	    			}); 
	    		} else {
	    			alert('Not Authorised!');
	    		}
	    	}
	    }
	};
	
	$scope.findPlant = function(link) {
		var plantId = link.substring(link.lastIndexOf("/")+1, link.length);
		$http.get('/rest/plants/'+plantId).success(function(data, status, headers, config) {
			console.log(JSON.stringify(data));
			$scope.plant = data;
			$scope.plantShown=true;
		});	
	};
});

app.controller('UpdatePHRController', function($scope, $http, $route, $location, $rootScope, $cookieStore) {	
	$scope.phrs = [];
	$http.get($scope.updateUrl).success(function(data, status, headers, config) {  
			 console.log(data);			 
			 $scope.phrs =data;		   		    
		     console.log($scope.phrs)
		     $scope.startDate =new Date( $scope.phrs.startDate);
		     $scope.endDate =  new Date( $scope.phrs.endDate);			 
		   });
	
	
	$scope.submit = function () {
		console.log($scope.updateUrl);
		phr = {startDate: $scope.startDate, endDate: $scope.endDate};
			$http.put ($scope.updateUrl,phr).success(function(data, status, headers, config) {
				console.log(data.id);
			    $location.path('/phrs');
			   });
	    };
});

app.controller('ExtendPHRController', function($scope, $http, $route, $location, $rootScope, $cookieStore) {	
	$scope.phrs = [];
	$http.get($scope.getUrl).success(function(data, status, headers, config) {  
			 console.log(data);			 
			 $scope.phrs =data;		   		    
		     console.log($scope.phrs)
		     $scope.startDate =new Date( $scope.phrs.startDate);
		     $scope.endDate =  new Date( $scope.phrs.endDate);			 
		   });
	
	
	$scope.submit = function () {
		console.log($scope.extendUrl);
		phr = {startDate: $scope.startDate, endDate: $scope.endDate};
			$http.put ($scope.extendUrl,phr).success(function(data, status, headers, config) {
				console.log(data.id);
			    $location.path('/phrs');
			   });
	    };
});

app.controller('CreatePHRController', function($scope, $http, $location) {
    $scope.plants = [];
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();

    $scope.catalogShown = false;

    $scope.execQuery = function () {
        $http.get('/rest/plants/availability', {params: {name: $scope.name, startDate: $scope.startDate, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
            $scope.plants = data;
            $scope.catalogShown = true;
        });
    };
    $scope.setPlant = function (selectedPlant) {
    	console.log(selectedPlant.links[0].href);
        phr = { plantRef: selectedPlant.links[0].href, startDate: $scope.startDate, endDate: $scope.endDate};

        $http.post('/rest/phrs', phr).success(function(data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };
});

app.controller('StatusController', function($scope, $http, $location) {
    $scope.status = '';
    $scope.id = null;
    $scope.statusShown = false;

    $scope.execQuery = function () {
    	var url = '/rest/phrs/'+$scope.id+'/status';
    	console.log(url);
        $http.get(url).success(function(data, status, headers, config) {
            $scope.status = data;
            $scope.statusShown = true;
        }).error(function(data, status, headers, config) {
        	alert(status);
          });
    };
});

app.controller("LoginController", function($scope, $http, $location, $rootScope, $cookieStore) {
	$scope.username = "";
    $scope.password = "";
    $scope.role = "";
    delete $rootScope.user;
    delete $rootScope.code;
    delete $rootScope.roles;
    $cookieStore.remove("user");
    $cookieStore.remove("code");                
    $cookieStore.remove("userRoles");

    $scope.login = function () {
        var code = window.btoa($scope.username + ":" + $scope.password);      
        console.log(code);
        $rootScope.user = $scope.username;
        $rootScope.code = code;
        $http.post("/rest/authentication")
            .success(function(data, status, headers, config) {            
            	console.log(data.roles);
            	$rootScope.role = data.roles;
            	$cookieStore.put("userRoles", data.roles);
                $cookieStore.put("user", $scope.username);
                $cookieStore.put("code", code); 
                $rootScope.roles = data.roles;
                $location.path( "/home" );
            }).
            error(function(data, status, headers, config) {
            	                       
                $location.path( "/login" );              
                return false;
              });
    };
});

app.controller("HomeController", function($rootScope, $location, $cookieStore,$location) {
	
});