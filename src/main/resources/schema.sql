create table if not exists users (username varchar(50) not null primary key, password varchar(50) not null, enabled boolean not null)
create table if not exists authorities (username varchar(50) not null, authority varchar(50) not null, constraint FK_AUTHORITIES_USERS foreign key(username) references users(username))


insert into users (username, password, enabled) SELECT 'admin', 'admin', true where not exists (select username, password, enabled from users where username = 'admin' and password = 'admin')
insert into authorities (username, authority) SELECT 'admin', 'ROLE_ADMIN' where not exists (select username, authority from authorities where username = 'admin' and authority = 'ROLE_ADMIN')

insert into users (username, password, enabled) SELECT 'site', 'site', true where not exists (select username, password, enabled from users where username = 'site' and password = 'site')
insert into authorities (username, authority) SELECT 'site', 'ROLE_SITE' where not exists (select username, authority from authorities where username = 'site' and authority = 'ROLE_SITE')

insert into users (username, password, enabled) SELECT 'work', 'work', true where not exists (select username, password, enabled from users where username = 'work' and password = 'work')
insert into authorities (username, authority) SELECT 'work', 'ROLE_WORK' where not exists (select username, authority from authorities where username = 'work' and authority = 'ROLE_WORK')
