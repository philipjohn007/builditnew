package builditnew.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import builditnew.integration.rest.NotificationRestController;
import builditnew.models.Notification;

public class NotificationResourceAssembler extends
		ResourceAssemblerSupport<Notification, NotificationResource> {

	public NotificationResourceAssembler() {
		super(NotificationRestController.class, NotificationResource.class);
	}

	@Override
	public NotificationResource toResource(Notification notify) {
		NotificationResource res = createResourceWithId(notify.getId(), notify);

		res.setMessage(notify.getMessage());
		res.setPoRef(notify.getPoRef());
		res.setPrice(notify.getPrice());

		return res;
	}

	public List<NotificationResource> toResource(
			final List<Notification> notifications) {
		List<NotificationResource> ress = new ArrayList<NotificationResource>();

		for (final Notification notify : notifications) {
			ress.add(toResource(notify));
		}
		return ress;
	}
}
