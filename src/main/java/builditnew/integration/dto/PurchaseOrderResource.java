/**
 *
 */
package builditnew.integration.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import builditnew.util.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author philipjohn007
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PurchaseOrderResource extends ResourceSupport {
	PlantResource plant;

	@DateTimeFormat(iso = ISO.DATE)
	Date startDate;
	@DateTimeFormat(iso = ISO.DATE)
	Date endDate;

	String status;

	Float cost;
}
