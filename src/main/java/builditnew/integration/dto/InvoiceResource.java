/**
 *
 */
package builditnew.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import builditnew.models.InvoiceStatus;
import builditnew.util.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "invoice")
public class InvoiceResource extends ResourceSupport {

	Long idRes;
	String poRef;
	InvoiceStatus status;
	Float price;
}
