/**
 *
 */
package builditnew.integration.dto;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import builditnew.integration.rest.PHRNotFoundException;
import builditnew.integration.rest.PlantHireRequestRestController;
import builditnew.models.PlantHireRequest;
import builditnew.util.ExtendedLink;

/**
 * @author philipjohn007
 *
 */
public class PlantHireRequestAssembler extends
		ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestResource> {

	public PlantHireRequestAssembler() {
		super(PlantHireRequestRestController.class,
				PlantHireRequestResource.class);
	}

	@Override
	public PlantHireRequestResource toResource(final PlantHireRequest plantHire) {
		final PlantHireRequestResource res = createResourceWithId(
				plantHire.getId(), plantHire);

		res.setEndDate(plantHire.getEndDate());
		res.setStartDate(plantHire.getStartDate());
		res.setPlantRef(plantHire.getPlantRef());
		res.setSiteEngineer(plantHire.getSiteEngineer());
		res.setWorkEngineer(plantHire.getWorkEngineer());
		res.setPrice(plantHire.getPrice());
		res.setPoRef(plantHire.getPurchaseOrderRef());

		try {
			switch (plantHire.getStatus()) {
			case PENDING_CONFIRMATION:
				res.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class)
								.acceptPHR(plantHire.getId())).toString(),
						"Accept", "POST"));
				res.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class)
								.rejectPHR(plantHire.getId())).toString(),
						"Reject", "DELETE"));
				res.add(new ExtendedLink(
						linkTo(
								methodOn(PlantHireRequestRestController.class)
										.updatePHR(plantHire.getId(), null))
								.toString(), "Update", "PUT"));
				break;
			case REJECTED:
				res.add(new ExtendedLink(
						linkTo(
								methodOn(PlantHireRequestRestController.class)
										.updatePHR(plantHire.getId(), null))
								.toString(), "Update", "PUT"));
				break;
			case CANCELLED:
				break;
			case APPROVED:
				res.add(new ExtendedLink(
						linkTo(
								methodOn(PlantHireRequestRestController.class)
										.extendPHR(plantHire.getId(), null))
								.toString(), "Extend", "PUT"));
				res.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class)
								.cancelPHR(plantHire.getId())).toString(),
						"Cancel", "DELETE"));
				break;
			default:
				break;
			}
		} catch (final PHRNotFoundException e) {
			System.out.println("PHR Not Found!" + e);
		}
		return res;
	}

	public List<PlantHireRequestResource> toResource(
			final List<PlantHireRequest> plants) {
		final List<PlantHireRequestResource> ress = new ArrayList<>();

		for (final PlantHireRequest plant : plants) {
			ress.add(toResource(plant));
		}
		return ress;
	}
}
