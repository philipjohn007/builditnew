/**
 *
 */
package builditnew.integration.dto;

import lombok.Getter;
import lombok.Setter;
import builditnew.util.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author philipjohn007
 *
 */
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PlantResource extends ResourceSupport {
	Long idRes;
	String name;
	String description;
	Float price;
}
