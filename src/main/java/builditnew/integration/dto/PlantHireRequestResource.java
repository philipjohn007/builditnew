/**
 *
 */
package builditnew.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import builditnew.models.SiteEngineer;
import builditnew.models.WorkEngineer;
import builditnew.util.ResourceSupport;

/**
 * @author philipjohn007
 *
 */
@Getter
@Setter
@XmlRootElement(name = "plant_hire_request")
public class PlantHireRequestResource extends ResourceSupport {

	Long idRes;
	String plantRef;
	SiteEngineer siteEngineer;
	WorkEngineer workEngineer;
	Date startDate;
	Date endDate;
	Float price;
	String poRef;
}
