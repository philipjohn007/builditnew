/**
 *
 */
package builditnew.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import builditnew.util.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "notification")
public class NotificationResource extends ResourceSupport {

	String message;
	String poRef;
	Float price;
}
