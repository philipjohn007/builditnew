package builditnew.integration.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import builditnew.integration.dto.PlantResource;
import builditnew.services.rentit.RentalService;

/**
 * @author philipjohn007
 *
 */
@RestController
@RequestMapping(value = "/rest/plants")
public class PlantRestController {

	@Autowired
	private RentalService rentItProxy;

	@RequestMapping
	@ResponseStatus(HttpStatus.OK)
	public List<PlantResource> getAll() {

		return rentItProxy.getPlants();
	}

	@RequestMapping(value = "{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlantResource getPlant(@PathVariable("id") final Long id)
			throws Exception {
		if (id == null) {
			return null;
		}
		return rentItProxy.findPlant(id);
	}

	@RequestMapping(value = "/availability", method = RequestMethod.GET)
	public List<PlantResource> findAllAvailablePlants(
			@RequestParam(value = "name") String name,
			@RequestParam(value = "startDate") String startDate,
			@RequestParam(value = "endDate") String endDate) {
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date1 = formatter.parse(startDate);
			Date date2 = formatter.parse(endDate);
			List<PlantResource> resources = rentItProxy.findAvailablePlants(
					name, date1, date2);
			return resources;
		} catch (ParseException e) {
			return null;
		}
	}
}
