package builditnew.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import builditnew.integration.dto.InvoiceResource;
import builditnew.integration.dto.InvoiceResourceAssembler;
import builditnew.models.Invoice;
import builditnew.models.InvoiceStatus;
import builditnew.repositories.InvoiceRepository;
import builditnew.services.rentit.RentalService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest/invoices")
public class InvoiceRestController {

	InvoiceResourceAssembler assembler = new InvoiceResourceAssembler();

	@Autowired
	InvoiceRepository repo;

	@Autowired
	private RentalService rentItProxy;

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public List<InvoiceResource> getAllPHRs() {
		final List<InvoiceResource> resources = assembler.toResource(repo
				.findAll());
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resources));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resources;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public InvoiceResource getPHR(@PathVariable final Long id)
			throws InvoiceNotFoundException {
		Invoice invoice = repo.findOne(id);
		if (invoice == null) {
			throw new InvoiceNotFoundException(id);
		}
		final InvoiceResource resource = assembler.toResource(invoice);
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resource));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resource;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public InvoiceResource createInvoice(@RequestBody final InvoiceResource res) {
		System.out.println("Request");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final Invoice invoice = new Invoice();
		invoice.setPrice(res.getPrice());
		invoice.setStatus(InvoiceStatus.PENDING);
		invoice.setPoRef(res.getPoRef());

		final Invoice saved = repo.saveAndFlush(invoice);
		final InvoiceResource savedRes = assembler.toResource(saved);
		System.out.println("Response");
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@RequestMapping(method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public InvoiceResource updateInvoice(@RequestBody final InvoiceResource res) {
		if (res == null) {
			return null;
		}
		System.out.println("Request");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Invoice invoice = getInvoiceUsingPoRef(res.getPoRef());
		invoice.setPrice(res.getPrice());
		invoice.setStatus(InvoiceStatus.PENDING);
		Invoice saved = repo.save(invoice);
		InvoiceResource savedRes = assembler.toResource(saved);
		System.out.println("Response");
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST, value = "/{id}/accept")
	public InvoiceResource approve(@PathVariable Long id)
			throws InvoiceNotFoundException {

		Invoice invoice = repo.findOne(id);
		if (invoice == null) {
			throw new InvoiceNotFoundException(id);
		}
		invoice.setStatus(InvoiceStatus.APPROVED);
		Invoice saved = repo.save(invoice);
		InvoiceResource res = assembler.toResource(saved);
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rentItProxy.updateInvoiceStatus(res);
		return res;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}/accept")
	public InvoiceResource reject(@PathVariable Long id)
			throws InvoiceNotFoundException {
		Invoice invoice = repo.findOne(id);
		if (invoice == null) {
			throw new InvoiceNotFoundException(id);
		}
		invoice.setStatus(InvoiceStatus.REJECTED);
		Invoice saved = repo.save(invoice);
		InvoiceResource res = assembler.toResource(saved);
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rentItProxy.updateInvoiceStatus(res);
		return res;
	}

	public Invoice getInvoiceUsingPoRef(String poRef) {
		List<Invoice> invoices = repo.findAll();
		if (invoices == null) {
			return null;
		}
		for (Invoice invoice : invoices) {
			if (invoice.getPoRef().equals(poRef)) {
				return invoice;
			}
		}
		return null;
	}

	@ExceptionHandler(InvoiceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleInvoiceNotFoundException(final InvoiceNotFoundException ex) {
	}
}
