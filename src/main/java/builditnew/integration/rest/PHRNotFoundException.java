/**
 *
 */
package builditnew.integration.rest;

/**
 * @author philipjohn007
 *
 */
public class PHRNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public PHRNotFoundException(final Long id) {
		super(String.format("Plant Hire Request not found! (PHR id: %d)", id));
	}
}
