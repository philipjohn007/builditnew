package builditnew.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import builditnew.integration.dto.NotificationResource;
import builditnew.integration.dto.NotificationResourceAssembler;
import builditnew.models.Notification;
import builditnew.repositories.NotificationRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest/notifications")
public class NotificationRestController {

	NotificationResourceAssembler assembler = new NotificationResourceAssembler();

	@Autowired
	NotificationRepository repo;

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public List<NotificationResource> getAllNotifications() {
		List<NotificationResource> resources = assembler.toResource(repo
				.findAll());
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resources));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resources;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public NotificationResource createNotification(
			@RequestBody final NotificationResource res) {
		System.out.println("Request");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Notification notification = new Notification();
		notification.setPrice(res.getPrice());
		notification.setPoRef(res.getPoRef());
		notification.setMessage(res.getMessage());

		Notification saved = repo.saveAndFlush(notification);
		NotificationResource savedRes = assembler.toResource(saved);
		System.out.println("Response");
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

}
