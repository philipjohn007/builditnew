/**
 *
 */
package builditnew.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import builditnew.integration.dto.PurchaseOrderResource;
import builditnew.services.rentit.RentalService;

/**
 * @author philipjohn007
 *
 */
@RestController
@RequestMapping(value = "/rest/pos")
public class PurchaseOrderRestController {

	@Autowired
	private RentalService rentItProxy;

	@RequestMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource getPO(@PathVariable("id") final Long id)
			throws Exception {
		if (id == null) {
			return null;
		}
		return rentItProxy.findPO(id);
	}

	@RequestMapping
	@ResponseStatus(HttpStatus.OK)
	public List<PurchaseOrderResource> getAll() {
		return rentItProxy.getPOs();
	}
}
