/**
 *
 */
package builditnew.integration.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import builditnew.integration.dto.PlantHireRequestAssembler;
import builditnew.integration.dto.PlantHireRequestResource;
import builditnew.integration.dto.PlantResource;
import builditnew.integration.dto.PurchaseOrderResource;
import builditnew.models.PHRStatus;
import builditnew.models.PlantHireRequest;
import builditnew.repositories.PlantHireRequestRepository;
import builditnew.services.PlantNotAvailableException;
import builditnew.services.rentit.RentalService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest/phrs")
public class PlantHireRequestRestController {

	PlantHireRequestAssembler assembler = new PlantHireRequestAssembler();

	@Autowired
	PlantHireRequestRepository repo;

	@Autowired
	private RentalService rentItProxy;

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public List<PlantHireRequestResource> getAllPHRs() {
		final List<PlantHireRequestResource> resources = assembler
				.toResource(repo.findAll());
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resources));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resources;
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public PlantHireRequestResource getPHR(@PathVariable final Long id)
			throws PHRNotFoundException {
		PlantHireRequest phr = repo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(id);
		}
		final PlantHireRequestResource resource = assembler.toResource(phr);
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(resource));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resource;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public PlantHireRequestResource createPHR(
			@RequestBody final PlantHireRequestResource res) {
		System.out.println("Request");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final PlantHireRequest request = new PlantHireRequest();
		request.setEndDate(res.getEndDate());
		request.setStartDate(res.getStartDate());
		request.setPrice(res.getPrice());
		request.setSiteEngineer(res.getSiteEngineer());
		request.setWorkEngineer(res.getWorkEngineer());
		request.setStatus(PHRStatus.PENDING_CONFIRMATION);
		request.setPlantRef(res.getPlantRef());

		final PlantHireRequest savedPHR = repo.saveAndFlush(request);
		final PlantHireRequestResource savedRes = assembler
				.toResource(savedPHR);
		System.out.println("Response");
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/accept")
	@ResponseStatus(value = HttpStatus.OK)
	public PlantHireRequestResource acceptPHR(@PathVariable final Long id)
			throws PHRNotFoundException, RestClientException {
		final PlantHireRequest phr = repo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(id);
		}
		phr.setStatus(PHRStatus.APPROVED);

		String plantRef = phr.getPlantRef();
		Long plantId = getIdFromLink(plantRef);
		PlantResource plant = rentItProxy.findPlant(plantId);
		if (plant == null) {
			return null;
		}

		PurchaseOrderResource createdPO = null;
		try {
			createdPO = rentItProxy.createPurchaseOrder(plant,
					phr.getStartDate(), phr.getEndDate());
			if (createdPO != null) {
				phr.setPurchaseOrderRef(createdPO.getLink("self").getHref());
				phr.setPrice(createdPO.getCost());
			}
		} catch (PlantNotAvailableException e) {
			phr.setStatus(PHRStatus.REJECTED);
		}

		final PlantHireRequest saved = repo.save(phr);

		PlantHireRequestResource savedRes = assembler.toResource(saved);

		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return savedRes;

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}/accept")
	@ResponseStatus(value = HttpStatus.OK)
	public PlantHireRequestResource rejectPHR(@PathVariable final Long id)
			throws PHRNotFoundException {
		final PlantHireRequest phr = repo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(id);
		}
		phr.setStatus(PHRStatus.REJECTED);
		final PlantHireRequest saved = repo.save(phr);
		PlantHireRequestResource savedRes = assembler.toResource(saved);
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public PlantHireRequestResource updatePHR(@PathVariable final Long id,
			@RequestBody final PlantHireRequestResource res)
			throws PHRNotFoundException {
		final PlantHireRequest phr = repo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(id);
		}
		System.out.println("Request");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		phr.setEndDate(res.getEndDate());
		phr.setStartDate(res.getStartDate());
		phr.setStatus(PHRStatus.PENDING_CONFIRMATION);
		final PlantHireRequest saved = repo.save(phr);
		PlantHireRequestResource savedRes = assembler.toResource(saved);
		System.out.println("Response");
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/extend")
	@ResponseStatus(value = HttpStatus.OK)
	public PlantHireRequestResource extendPHR(@PathVariable final Long id,
			@RequestBody final PlantHireRequestResource res)
			throws PHRNotFoundException {
		final PlantHireRequest phr = repo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(id);
		}

		System.out.println("Request");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(res));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String plantRef = phr.getPlantRef();
		Long plantId = getIdFromLink(plantRef);
		PlantResource plant = rentItProxy.findPlant(plantId);
		if (plant == null) {
			return null;
		}

		String poRef = phr.getPurchaseOrderRef();
		Long poId = getIdFromLink(poRef);

		PurchaseOrderResource updatedPO = null;
		try {
			updatedPO = rentItProxy.updatePurchaseOrder(poId, plant,
					res.getStartDate(), res.getEndDate());
			if (updatedPO != null) {
				phr.setPurchaseOrderRef(updatedPO.getLink("self").getHref());
				phr.setPrice(updatedPO.getCost());
				phr.setStatus(PHRStatus.APPROVED);
				phr.setStartDate(res.getStartDate());
				phr.setEndDate(res.getEndDate());
				final PlantHireRequest saved = repo.save(phr);
				PlantHireRequestResource savedRes = assembler.toResource(saved);
				System.out.println("Response");
				try {
					System.out.println(mapper.writeValueAsString(savedRes));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return savedRes;
			}
		} catch (PlantNotAvailableException e) {
			return null;
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public PlantHireRequestResource cancelPHR(@PathVariable final Long id)
			throws PHRNotFoundException {
		final PlantHireRequest phr = repo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(id);
		}
		phr.setStatus(PHRStatus.CANCELLED);
		String poRef = phr.getPurchaseOrderRef();
		Long poId = getIdFromLink(poRef);
		rentItProxy.cancelPurchaseOrder(poId);
		final PlantHireRequest saved = repo.save(phr);
		PlantHireRequestResource savedRes = assembler.toResource(saved);
		System.out.println("Response");
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(savedRes));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return savedRes;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/updates")
	@ResponseStatus(value = HttpStatus.OK)
	public PlantHireRequestResource requestPHRUpdate(@PathVariable final Long id)
			throws PHRNotFoundException {
		final PlantHireRequest phr = repo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(id);
		}
		phr.setStatus(PHRStatus.PENDING_CONFIRMATION);
		final PlantHireRequest saved = repo.save(phr);
		return assembler.toResource(saved);
	}

	@RequestMapping(value = "/{id}/status", method = RequestMethod.GET)
	public PHRStatus checkStatus(@PathVariable("id") final Long id) {
		final PlantHireRequest phr = repo.findOne(id);
		if (phr != null) {
			return phr.getStatus();
		}
		return null;
	}

	@ExceptionHandler(PHRNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPHRNotFoundException(final PHRNotFoundException ex) {
	}

	public Long getIdFromLink(String link) {
		if (link == null || link == "") {
			return null;
		}
		String plantId = link.substring(link.lastIndexOf("/") + 1,
				link.length());
		return Long.parseLong(plantId);
	}

}
