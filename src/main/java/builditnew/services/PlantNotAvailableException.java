/**
 * 
 */
package builditnew.services;

/**
 * @author philipjohn007
 *
 */
public class PlantNotAvailableException extends Exception {
	private static final long serialVersionUID = 1L;

	public PlantNotAvailableException() {
		super();
	}
}
