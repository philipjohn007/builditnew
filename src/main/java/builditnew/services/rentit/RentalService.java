/**
 *
 */
package builditnew.services.rentit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import builditnew.integration.dto.InvoiceResource;
import builditnew.integration.dto.PlantResource;
import builditnew.integration.dto.PurchaseOrderResource;
import builditnew.services.PlantNotAvailableException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RentalService {
	@Autowired
	RestTemplate restTemplate;

	@Value("${rentit1.host}")
	String host1;

	@Value("${rentit1.port}")
	String port1;

	@Value("${rentit2.host}")
	String host2;

	@Value("${rentit2.port}")
	String port2;

	@Value("${rentit3.host}")
	String host3;

	@Value("${rentit3.port}")
	String port3;

	public PlantResource findPlant(Long id) {
		final ResponseEntity<PlantResource> result = restTemplate.getForEntity(
				"http://" + host1 + port1 + "/rest/plants/" + id,
				PlantResource.class);
		if (result == null) {
			return null;
		}
		return result.getBody();
	}

	public PurchaseOrderResource findPO(Long id) {
		final ResponseEntity<PurchaseOrderResource> result = restTemplate
				.getForEntity("http://" + host1 + port1 + "/rest/pos/" + id,
						PurchaseOrderResource.class);
		if (result == null) {
			return null;
		}
		return result.getBody();
	}

	public List<PlantResource> getPlants() {
		PlantResource[] result1 = restTemplate.getForObject("http://" + host1
				+ port1 + "/rest/plants/", PlantResource[].class);
		if (result1 == null) {
			return null;
		}
		List<PlantResource> resources = new ArrayList<PlantResource>();
		resources.addAll(Arrays.asList(result1));
		result1 = restTemplate.getForObject("http://" + host2 + port2
				+ "/rest/plants", PlantResource[].class);
		resources.addAll(Arrays.asList(result1));
		result1 = restTemplate.getForObject("http://" + host3 + port3
				+ "/rest/plants", PlantResource[].class);
		resources.addAll(Arrays.asList(result1));
		return resources;
	}

	public List<PurchaseOrderResource> getPOs() {
		final PurchaseOrderResource[] result = restTemplate.getForObject(
				"http://" + host1 + port1 + "/rest/pos/",
				PurchaseOrderResource[].class);
		if (result == null) {
			return null;
		}
		return Arrays.asList(result);
	}

	public List<PlantResource> findAvailablePlants(final String plantName,
			final Date startDate, final Date endDate) {
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		final PlantResource[] plants = restTemplate
				.getForObject(
						"http://"
								+ host1
								+ port1
								+ "/rest/plants/availability?name={name}&startDate={start}&endDate={end}",
						PlantResource[].class, plantName,
						formatter.format(startDate), formatter.format(endDate));
		return Arrays.asList(plants);
	}

	public PurchaseOrderResource createPurchaseOrder(final PlantResource plant,
			final Date startDate, final Date endDate)
			throws PlantNotAvailableException {
		final PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(plant);
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		try {
			ObjectMapper mapper = new ObjectMapper();
			try {
				System.out.println(mapper.writeValueAsString(po));
			} catch (JsonProcessingException e) {

			}
			final ResponseEntity<PurchaseOrderResource> result = restTemplate
					.postForEntity("http://" + host1 + port1 + "/rest/pos", po,
							PurchaseOrderResource.class);

			if (result.getStatusCode().equals(HttpStatus.CONFLICT)) {
				throw new PlantNotAvailableException();
			}

			return result.getBody();
		} catch (final HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.CONFLICT)) {
				throw new PlantNotAvailableException();
			}
			return null;
		}
	}

	public void cancelPurchaseOrder(Long id) {
		try {
			restTemplate.delete("http://" + host1 + port1 + "/rest/pos/" + id,
					PurchaseOrderResource.class);
		} catch (final HttpClientErrorException e) {
		}
	}

	public PurchaseOrderResource updatePurchaseOrder(Long poId,
			PlantResource plant, final Date startDate, final Date endDate)
			throws RestClientException, PlantNotAvailableException {

		final ResponseEntity<PurchaseOrderResource> result = restTemplate
				.getForEntity("http://" + host1 + port1 + "/rest/pos/" + poId,
						PurchaseOrderResource.class);
		if (result == null) {
			throw new PlantNotAvailableException();
		}

		final PurchaseOrderResource po = result.getBody();
		po.setPlant(plant);
		po.setStartDate(startDate);
		po.setEndDate(endDate);

		try {
			final ResponseEntity<PurchaseOrderResource> updatedResult = restTemplate
					.postForEntity("http://" + host1 + port1 + "/rest/pos/"
							+ poId + "/extend", po, PurchaseOrderResource.class);

			if (updatedResult.getStatusCode().equals(HttpStatus.CONFLICT)) {
				throw new PlantNotAvailableException();
			}

			return updatedResult.getBody();

		} catch (final RestClientException e) {
			return null;
		}
	}

	public void updateInvoiceStatus(InvoiceResource res) {
		try {
			restTemplate.put("http://" + host1 + port1 + "/rest/invoices", res,
					InvoiceResource.class);

		} catch (final RestClientException e) {
			return;
		}
	}
}
