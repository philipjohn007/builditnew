/**
 *
 */
package builditnew.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author philipjohn007
 *
 */

@RestController
@RequestMapping("/phrs")
public class PHRController {

	@RequestMapping(value = "/form")
	public String getForm() {
		return "phrs/query_plant_catalog";
	}
}
