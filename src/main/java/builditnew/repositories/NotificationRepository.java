/**
 *
 */
package builditnew.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import builditnew.models.Notification;

/**
 * @author philipjohn007
 *
 */
@Repository
public interface NotificationRepository extends
		JpaRepository<Notification, Long> {

}
