/**
 *
 */
package builditnew.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import builditnew.models.PlantHireRequest;

/**
 * @author philipjohn007
 *
 */
@Repository
public interface PlantHireRequestRepository extends
		JpaRepository<PlantHireRequest, Long> {

}
