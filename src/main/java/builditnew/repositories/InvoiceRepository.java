/**
 *
 */
package builditnew.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import builditnew.models.Invoice;

/**
 * @author philipjohn007
 *
 */
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

}
