package builditnew;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

	@Autowired
	private WebMvcProperties mvcProperties = new WebMvcProperties();
	@Autowired
	Credentials credentials;
	@Autowired
	ClientHttpRequestFactory basicSecure;

	class CustomResponseErrorHandler implements ResponseErrorHandler {
		private ResponseErrorHandler errorHandler = new DefaultResponseErrorHandler();

		@Override
		public boolean hasError(ClientHttpResponse response) throws IOException {
			return errorHandler.hasError(response);
		}

		@Override
		public void handleError(ClientHttpResponse response) throws IOException {
		}
	}

	public class BasicSecureSimpleClientHttpRequestFactory extends
			SimpleClientHttpRequestFactory {
		@Autowired
		Credentials credentials;

		public BasicSecureSimpleClientHttpRequestFactory() {
		}

		@Override
		public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod)
				throws IOException {
			ClientHttpRequest result = super.createRequest(uri, httpMethod);
			System.out.println(uri);
			System.out.println(uri.getAuthority());
			System.out.println(credentials.getCredentials());

			for (Map<String, String> map : credentials.getCredentials()
					.values()) {
				String authority = map.get("authority");
				if (authority != null && authority.equals(uri.getAuthority())) {
					result.getHeaders().add("Authorization",
							map.get("authorization"));
					break;
				}
			}

			if (credentials.getCredentials().containsKey(uri.getAuthority())) {
			}
			return result;
		}
	}

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate _restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new MappingJackson2HttpMessageConverter());
		_restTemplate.setMessageConverters(messageConverters);
		_restTemplate.setErrorHandler(new CustomResponseErrorHandler());
		_restTemplate.setRequestFactory(basicSecure);
		return _restTemplate;
	}

	@Bean
	public ClientHttpRequestFactory requestFactory() {
		return new BasicSecureSimpleClientHttpRequestFactory();
	}

	@Bean
	@ConfigurationProperties(locations = "classpath:META-INF/integration/credentials.yml")
	public Credentials getCredentials() {
		return new Credentials();
	}

	public static class Credentials {
		private Map<String, Map<String, String>> credentials = new HashMap<>();

		public Map<String, Map<String, String>> getCredentials() {
			return this.credentials;
		}
	}

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}
}
