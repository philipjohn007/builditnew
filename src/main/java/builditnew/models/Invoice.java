/**
 *
 */
package builditnew.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author philipjohn007
 *
 */
@Entity
@Data
@XmlRootElement
public class Invoice {
	@Id
	@GeneratedValue
	Long id;

	String poRef;

	@Enumerated(EnumType.STRING)
	InvoiceStatus status;

	Float price;
}
