/**
 *
 */
package builditnew.models;

/**
 * @author philipjohn007
 *
 */
public enum PHRStatus {

	PENDING_CONFIRMATION,

	APPROVED,

	REJECTED,

	CANCELLED;
}
