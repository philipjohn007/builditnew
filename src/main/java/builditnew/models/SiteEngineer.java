/**
 *
 */
package builditnew.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

/**
 * @author philipjohn007
 *
 */
@Entity
@Data
public class SiteEngineer {
	@Id
	@GeneratedValue
	Long id;

	String name;
}
