/**
 *
 */
package builditnew.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author philipjohn007
 *
 */
@Entity
@Data
@XmlRootElement
public class Notification {
	@Id
	@GeneratedValue
	Long id;

	String message;
	String poRef;
	Float price;
}
