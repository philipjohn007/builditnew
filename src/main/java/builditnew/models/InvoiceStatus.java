/**
 *
 */
package builditnew.models;

/**
 * @author philipjohn007
 *
 */
public enum InvoiceStatus {

	PENDING,

	APPROVED,

	REJECTED;
}
