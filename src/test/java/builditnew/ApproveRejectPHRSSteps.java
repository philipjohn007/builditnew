package builditnew;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ApproveRejectPHRSSteps {

	WebDriver user = new ChromeDriver();

	@Given("^there are PHRs in PENDING_CONFIRMATION state$")
	public void there_are_PHRs_in_PENDING_CONFIRMATION_state() throws Throwable {
		user.get("http://localhost:8051/#/phrs");
	}

	@Given("^a user logs in as WorkEngineer$")
	public void a_user_logs_in_as_WorkEngineer() throws Throwable {
		user.get("http://localhost:8051/#/login");
	}

	@When("^the WorkEngineer selects one phr to reject$")
	public void the_WorkEngineer_selects_one_phr_to_reject() throws Throwable {
		user.get("http://localhost:8051/#/phrs/1");
	}

	@When("^the WorkEngineer rejects the PHR$")
	public void the_WorkEngineer_rejects_the_PHR() throws Throwable {
		user.get("http://localhost:8051/#/phrs/1");
		WebElement rejBtn = user.findElement(By.id("btn-danger"));
		rejBtn.click();
	}

	@Then("^the status of phr sould be REJECTED$")
	public void the_status_of_phr_sould_be_REJECTED() throws Throwable {
		user.get("http://localhost:8051/#/phrs");
	}

	@When("^the WorkEngineer selects one phr to approve$")
	public void the_WorkEngineer_selects_one_phr_to_approve() throws Throwable {
		user.get("http://localhost:8051/#/phrs/2");
	}

	@When("^the WorkEngineer approves the PHR$")
	public void the_WorkEngineer_approves_the_PHR() throws Throwable {
		user.get("http://localhost:8051/#/phrs/2");
		WebElement appBtn = user.findElement(By.id("btn-success"));
		appBtn.click();
	}

	@Then("^the status of phr sould be APPROVED$")
	public void the_status_of_phr_sould_be_APPROVED() throws Throwable {
		user.get("http://localhost:8051/#/phrs");
	}
}
