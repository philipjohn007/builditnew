Feature: In the application    
        the WorkEngineer can Approve or Reject PHRs.
	
	Scenario: WorkEngineer rejects PHR
	Given there are PHRs in PENDING_CONFIRMATION state
	And   a user logs in as WorkEngineer
	When  the WorkEngineer selects one phr to reject
	And   the WorkEngineer rejects the PHR
	Then  the status of phr sould be REJECTED
	
	Scenario: WorkEngineer approves PHR
	Given there are PHRs in PENDING_CONFIRMATION state
	And   a user logs in as WorkEngineer
	When  the WorkEngineer selects one phr to approve
	And   the WorkEngineer approves the PHR
	Then  the status of phr sould be APPROVED
	