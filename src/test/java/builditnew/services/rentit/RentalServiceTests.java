package builditnew.services.rentit;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import builditnew.Application;
import builditnew.SimpleDbConfig;
import builditnew.integration.dto.PlantResource;
import builditnew.integration.dto.PurchaseOrderResource;
import builditnew.services.PlantNotAvailableException;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class,
		SimpleDbConfig.class })
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
// @ActiveProfiles("test")
public class RentalServiceTests {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	RentalService rentitProxy;

	List<ClientHttpRequestInterceptor> interceptors;

	@Before
	public void setUp() {
		interceptors = restTemplate.getInterceptors();
	}

	@After
	public void tearDown() {
		restTemplate.setInterceptors(interceptors);
	}

	@Test
	public void testCreatePurchaseOrder() throws Exception {
		final PlantResource plantResource = new PlantResource();
		plantResource.add(new Link("http://localhost:8052/rest/plants/10001",
				"self"));

		final PurchaseOrderResource poResource = new PurchaseOrderResource();
		poResource.setPlant(plantResource);
		poResource.setStartDate(new LocalDate(2014, 11, 12).toDate());
		poResource.setEndDate(new LocalDate(2014, 11, 14).toDate());

		final PurchaseOrderResource result = rentitProxy.createPurchaseOrder(
				plantResource, new LocalDate(2014, 11, 12).toDate(),
				new LocalDate(2014, 11, 14).toDate());
		poResource
				.add(new Link("http://localhost:8052/rest/pos/10001", "self"));

		assertEquals(poResource, result);
	}

	@Test
	public void testRejection() throws RestClientException,
			PlantNotAvailableException {
		final ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			@Override
			public ClientHttpResponse intercept(final HttpRequest request,
					final byte[] body,
					final ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().add("prefer", "409");
				return execution.execute(request, body);
			}
		};
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		final PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:8052/rest/plants/10001"));
		final Date startDate = new LocalDate(2014, 11, 12).toDate();
		final Date endDate = new LocalDate(2014, 11, 14).toDate();

		final PurchaseOrderResource result = rentitProxy.createPurchaseOrder(
				plant, startDate, endDate);
		Assert.assertThat(result, is(nullValue()));
	}

	@Test
	public void testConfirmation() throws RestClientException,
			PlantNotAvailableException {
		final PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:8052/rest/plants/10001"));
		final Date startDate = new LocalDate(2014, 11, 12).toDate();
		final Date endDate = new LocalDate(2014, 11, 14).toDate();

		final PurchaseOrderResource result = rentitProxy.createPurchaseOrder(
				plant, startDate, endDate);

		Assert.assertThat(result, is(notNullValue()));
	}

	@Test
	public void testUpdateConfirmation() throws RestClientException,
			PlantNotAvailableException {
		final PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:8052/rest/plants/10001"));
		final Date startDate = new LocalDate(2014, 11, 12).toDate();
		final Date endDate = new LocalDate(2014, 11, 14).toDate();

		final PurchaseOrderResource result = rentitProxy.updatePurchaseOrder(
				new Long(10001), plant, startDate, endDate);

		Assert.assertThat(result, is(notNullValue()));
	}

	@Test
	public void testUpdateRejection() throws RestClientException,
			PlantNotAvailableException {
		final ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			@Override
			public ClientHttpResponse intercept(final HttpRequest request,
					final byte[] body,
					final ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().add("prefer", "409");
				return execution.execute(request, body);
			}
		};
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		final PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:8052/rest/plants/10001"));
		final Date startDate = new LocalDate(2014, 11, 12).toDate();
		final Date endDate = new LocalDate(2014, 11, 14).toDate();

		final PurchaseOrderResource result = rentitProxy.updatePurchaseOrder(
				new Long(10001), plant, startDate, endDate);
		Assert.assertThat(result, is(nullValue()));
	}

	@Test
	public void testFindAvailablePlants() {
		String plantName = "Excavator";
		List<PlantResource> result = rentitProxy.findAvailablePlants(plantName,
				new LocalDate(2014, 11, 12).toDate(), new LocalDate(2014, 11,
						14).toDate());
		assertThat(result.size(), is(5));
	}
}
