package builditnew;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ExtendPHRSSteps {

	WebDriver user = new ChromeDriver();

	@Given("^there are some phrs in APPROVED state$")
	public void there_are_some_phrs_in_APPROVED_state() throws Throwable {
		user.get("http://localhost:8051/#/phrs");
	}

	@Given("^a user logs in as SiteEngineer$")
	public void a_user_logs_in_as_SiteEngineer() throws Throwable {
		user.get("http://localhost:8051/#/login");
	}

	@When("^the SiteEngineer selects the phr to be extended$")
	public void the_SiteEngineer_selects_the_phr_to_be_extended()
			throws Throwable {
		user.get("http://localhost:8051/#/phrs/4");
	}

	@When("^the SiteEngineer provides the new end date$")
	public void the_SiteEngineer_provides_the_new_end_date() throws Throwable {
		user.get("http://localhost:8051/#/extend");
	}

	@Then("^the PHR should be in PENDING_CONFIRMATION state$")
	public void the_PHR_should_be_in_PENDING_CONFIRMATION_state()
			throws Throwable {
		user.get("http://localhost:8051/#/phrs");
	}
}
