/**
 *
 */
package builditnew.integration.rest;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import builditnew.Application;
import builditnew.SimpleDbConfig;
import builditnew.integration.dto.PlantHireRequestResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class,
		SimpleDbConfig.class })
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
// @ActiveProfiles("test")
public class PHRRestControllerIntegrationTests {
	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;
	private final ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	@DatabaseSetup(value = "EmptyDatabase.xml")
	public void testCreatePlantHireRequest() throws Exception {
		final PlantHireRequestResource phr = new PlantHireRequestResource();
		phr.setPlantRef("http://localhost:8080/rest/plants/10001");
		phr.setStartDate(new LocalDate(2014, 10, 6).toDate());
		phr.setEndDate(new LocalDate(2014, 10, 10).toDate());

		final MvcResult result = mockMvc
				.perform(
						post("/rest/phrs").contentType(
								MediaType.APPLICATION_JSON).content(
								mapper.writeValueAsString(phr)))
				.andExpect(status().isCreated())
				// Verify the cost associated with the PHR
				.andReturn();

		final PlantHireRequestResource phrp = mapper.readValue(result
				.getResponse().getContentAsString(),
				PlantHireRequestResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.getLinks().size(), is(1));
	}

	@Test
	@DatabaseSetup(value = "DatabaseWithPendingPHR.xml")
	public void testApprovePlantHireRequest() throws Exception {
		MvcResult result = mockMvc
				.perform(post("/rest/phrs/{id}/accept", 10001L))
				.andExpect(status().isOk()).andReturn();
		PlantHireRequestResource phrp = mapper.readValue(result.getResponse()
				.getContentAsString(), PlantHireRequestResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.getLinks().size(), is(1));
	}

	@Test
	@DatabaseSetup(value = "DatabaseWithPendingPHR.xml")
	public void testRejectPlantHireRequest() throws Exception {
		MvcResult result = mockMvc
				.perform(delete("/rest/phrs/{id}/accept", 10001L))
				.andExpect(status().isOk()).andReturn();
		PlantHireRequestResource phrp = mapper.readValue(result.getResponse()
				.getContentAsString(), PlantHireRequestResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.getLinks().size(), is(1));
	}
}