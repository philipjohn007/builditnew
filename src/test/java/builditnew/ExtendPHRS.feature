Feature: In the application    
    the WorkEngineer can approve PHR
    and the SiteEngineer can extend the PHR if needed.
	
	Scenario: WorkEngineer approves PHR
	Given there are PHRs in PENDING_CONFIRMATION state
	And   a user logs in as WorkEngineer
	When  the WorkEngineer selects one phr to approve
	And   the WorkEngineer approves the PHR
	Then  the status of phr sould be APPROVED
	
	Scenario: SiteEngineer extends approved PHR
	Given there are some phrs in APPROVED state
	And   a user logs in as SiteEngineer
	When  the SiteEngineer selects the phr to be extended  
	And   the SiteEngineer provides the new end date 
	Then  the PHR should be in PENDING_CONFIRMATION state
	