package builditnew;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CancelPHRSSteps {

	WebDriver user = new ChromeDriver();

	@When("^the SiteEngineer selects the phr to be cancelled$")
	public void the_SiteEngineer_selects_the_phr_to_be_cancelled()
			throws Throwable {
		user.get("http://localhost:8051/#/phrs/3");
	}

	@When("^the SiteEngineer cancels the PHR$")
	public void the_SiteEngineer_cancels_the_PHR() throws Throwable {
		user.get("http://localhost:8051/#/phrs");
		WebElement rejBtn = user.findElement(By.id("btn-danger"));
		rejBtn.click();
	}

	@Then("^the PHR should be in CANCELLED state$")
	public void the_PHR_should_be_in_CANCELLED_state() throws Throwable {
		user.get("http://localhost:8051/#/phrs");
	}
}
